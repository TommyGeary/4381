> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tommy Geary

### Homework Links

[A1 Readme.md](a1/README.md) "Assignment 1 Readme"

* Distributed version control with Bitbucket
* Development installation
* Chapter 1-2 Questions


[A2 Readme.md](a2/README.md) "Assignment 2 Readme"

* Create Recipe 
* Upload a2 to Bitbucket
* Chapter 3 Questions


[A3 Readme.md](a3/README.md) "Assignment 3 Readme"

* Create repositories for a3
* Create database and forward engineer
* Questions

[P1 Readme.md](p1/README.md) "Project 1 Readme"

* Create Business Card app
* Upload to bitbucket
* Questions

[A4 Readme.md](a4/README.md) "Assignment 4 Readme"

* Download and edit website files
* Upload to bitbucket
* Questions

[A5 Readme.md](a5/README.md) "Assignment 5 Readme"

* Server-side validation
* Upload to bitbucket
* Questions

[P2 Readme.md](p2/README.md) "Project 2 Readme"

* Edit and delete functionality 
* Upload to bitbucket
* Questions