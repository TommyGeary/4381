> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tommy Geary

### Assignment 5 Requirements:

*Three Parts*

1. Server-side validation
2. Upload assignment to bitbucket
3. Questions

#### README.md file should include the following items:

* Requirements
* Screenshots of app

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of A5 index screen*:

![A4 home screenshot](img/index.png)

*Screenshot of A5 invalid data*:

![A4 invalid data Screenshot](img/process.png)

