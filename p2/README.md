> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tommy Geary

### Project Requirements:

*Three Parts*

1. Edit and delete functionality and RSS feeds
2. Upload assignment to bitbucket
3. Questions

#### README.md file should include the following items:

* Requirements
* Screenshots of app

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of P2 Index screen*:

![P2 home screenshot](img/index.png)

*Screenshot of P2 edit petstore data*:

![P2 invalid data Screenshot](img/process.png)

*Screenshot of P2 invalid data*:

![P2 invalid data Screenshot](img/invalid.png)

*Screenshot of P2 home page*:

![P2 invalid data Screenshot](img/carousel.png)

*Screenshot of P2 rss feed*:

![P2 invalid data Screenshot](img/rss.png)

