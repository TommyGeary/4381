> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tommy Geary

### Assignment 2 Requirements:

*Three parts*

1. Create Recipe 
2. Upload a2 to Bitbucket
3. Chapter 3 Questions

#### README.md file should include the following items:

* Screenshot of first recipe screen
* Screenshot of second recipe screen


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of first screen for Recipe app*:

![Bruschetta recipe first screen](img/recipe1.png)

*Screenshot of first screen for Recipe app*:

![Bruschetta recipe second screen](img/recipe2.png)

