> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tommy Geary

### Project 1 Requirements:

*Three Parts*

1. Create Business Card app
2. Upload assignment to bitbucket
3. Questions

#### README.md file should include the following items:

* Requirements
* Screenshots of app

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of P1 Main*:

![P1 Main Screenshot](img/p1main.png)

*Screenshot of P1 Card*:

![P1 Card Screenshot](img/p1card.png)


